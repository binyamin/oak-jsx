# Oak JSX

JSX-to-string helpers for [Preact](https://preactjs.com/). Includes middleware
for [Oak](https://github.com/oakserver/oak).

Compatible with Deno Deploy.

## Usage

The file `/mod.ts` has three named exports:

- The **`ssr`** function renders a Preact component to static HTML.
- The **views** function returns middleware for
  [Oak](https://github.com/oakserver/oak).
- The **Head** component

### Render a File

To render a JSX or TSX file as HTML, use the `ctx.render` method. Pass the raw
component as the first argument. You may also specify a second argument, which
will be used as props. The method also sets the `Content-Type` header to
`text/html`.

```js
// Import the component
import HomePage from './views/home.jsx';

const app = new oak.Application();

app.get('/', async (ctx, next) => {
	// Render the component...
	ctx.render(HomePage, {
		// ... with props
		title: 'My Website',
	});

	await next();
});
```

### Set Global Props

You can set props globally using `app.state.globals`. The value is merged with
any props passed to `ctx.render`. In case of conflict, local props override the
global props.

## Contributing

All input is welcome; feel free to
[open an issue](https://gitlab.com/binyamin/oak-jsx/-/issues/new). Please
remember to be a [mensch](https://www.merriam-webster.com/dictionary/mensch). If
you want to program, you can browse
[the issue list](https://gitlab.com/binyamin/oak-jsx/-/issues).

## Legal

All source-code is provided under the terms of
[the MIT license](https://gitlab.com/binyamin/oak-jsx/-/blob/main/LICENSE).
Copyright 2023 Binyamin Aron Green.
