import { deepMerge, oak, preact } from '../deps.ts';
import { ssr } from './ssr.ts';

/**
 * JSX-to-string middleware for [oak](https://github.com/oakserver/oak),
 * using Preact.
 */
export function views(): oak.Middleware {
	return async (ctx, next) => {
		ctx.render = <P>(component: preact.ComponentType<P>, props?: P) => {
			const data = deepMerge(props ?? {}, ctx.state.globals ?? {}) as
				& P
				& preact.Attributes;
			const vnode = preact.h(component, data);
			ctx.response.body = ssr(vnode, ctx.state.globals);
			ctx.response.headers.set('Content-Type', 'text/html; charset=utf-8');
		};

		await next();
	};
}
