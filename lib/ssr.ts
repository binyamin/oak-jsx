import { preact, renderToString } from '../deps.ts';
import { parseHead } from './head.ts';

type TemplateOptions = ReturnType<typeof parseHead>;

function template({ head, body, attributes }: TemplateOptions) {
	if (!attributes.html.has('lang')) attributes.html.set('lang', 'en');

	return `<!DOCTYPE html>
<html ${attributes.html}>
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	${head.join('\n\t')}
</head>
<body ${attributes.body}>
	${body}
</body>
</html>`;
}

/**
 * Render a Preact component as static HTML
 *
 * @param component A valid Preact VNode
 * @returns The rendered HTML
 *
 * @example
 * ```jsx
 * const Button = (props) => <button>{props.label}</button>;
 *
 * ssr(Button); // Invalid
 * ssr(h(Button, { label: "Click Me" })); // Valid
 * ssr(Button({ label: "Click Me" })); // Valid
 * ```
 */
export function ssr(component: preact.VNode, context?: unknown) {
	const str = renderToString(component, context);
	const head = parseHead(str);
	return template(head);
}
