import { preact, renderToString } from '../deps.ts';

// Adapted from https://github.com/nanojsx/nano/blob/07bfd2b0c8cab423a032cd7303040dcca099adc7/deno_lib/components/helmet.ts

class Attributes extends Map<string, unknown> {
	toString() {
		let string = '';

		for (const [key, value] of this) {
			string += `${key}="${value}" `;
		}

		return string.trim();
	}
}

type PropValue = string | number | bigint | boolean | null | undefined;

interface HeadProps {
	[x: `html_${string}`]: PropValue;
	html_lang?: string;
	[y: `body_${string}`]: PropValue;
}

const HeadContents: preact.VNode[] = [];

export const Head: preact.FunctionalComponent<HeadProps> = function Head(
	props,
) {
	const list = [props.children].flat();

	for (const e of list) {
		if (!preact.isValidElement(e)) continue;

		if (e.key) {
			const prev = HeadContents.findIndex((v) => v.key === e.key);

			if (prev > -1) {
				HeadContents[prev] = e;
				continue;
			}
		} else if (e.type === 'title') {
			const prev = HeadContents.findIndex((v) => v.type === 'title');

			if (prev > -1) {
				HeadContents[prev] = e;
				continue;
			}
		}

		HeadContents.push(e);
	}

	return preact.h('Head', { ...props }, props.children);
};

export function parseHead(body: string) {
	const reg = {
		head: /<Head\b([^>]*)>((?:.|\r|\n)*?)<\/Head>/gm,
		attrs: /(?:(html|body)_)?([\w\-]+)="([^"]+)"/gm,
	};

	// Holding cell for elements & attributes
	const elements = new Set<string>();
	const attributes = {
		html: new Attributes(),
		body: new Attributes(),
	};

	// [1] Extract elements
	for (const e of HeadContents) {
		elements.add(renderToString(e));
	}

	// [2] Extract attributes
	for (const [_, attrs] of body.matchAll(reg.head)) {
		const matches = attrs.matchAll(reg.attrs);

		for (const [_, scope, key, value] of matches) {
			if (['html', 'body'].includes(scope)) {
				attributes[scope as 'html' | 'body'].set(key, value);
			}
		}
	}

	return {
		body: body.replace(reg.head, ''),
		head: Array.from(elements),
		attributes,
	};
}
