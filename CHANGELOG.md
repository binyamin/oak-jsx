# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project tries to adhere to
[Semantic Versioning (SemVer)](https://semver.org/spec/v2.0.0.html).

<!--
	**Added** for new features.
	**Changed** for changes in existing functionality.
	**Deprecated** for soon-to-be removed features.
	**Removed** for now removed features.
	**Fixed** for any bug fixes.
	**Security** in case of vulnerabilities.
-->

## [0.2.1] - 2023-06-18

### Changed

- Updated dependencies

## [0.2.0] - 2023-03-08

### Added

- Export _ssr_ function and _Head_ component in `mod.ts`

### Changed

- Updated dependencies
- In `mod.ts`, the Oak middleware function is now a named export, instead of the
  default export

## [0.1.1] - 2022-11-03

### Fixed

- De-duplicate elements when rendering the `Head` component

## [0.1.0] - 2022-11-03

### Added

- Render JSX/TSX as static HTML, via middleware
- Merge `app.state.globals` with local props
- Provide a `Head` utility component
- Provide an `ssr` function, for advanced usage

[0.1.0]: https://gitlab.com/binyamin/oak-jsx/-/commits/v0.1.0
[0.1.1]: https://gitlab.com/binyamin/oak-jsx/-/commits/v0.1.1
[0.2.0]: https://gitlab.com/binyamin/oak-jsx/-/commits/v0.2.0
[0.2.1]: https://gitlab.com/binyamin/oak-jsx/-/commits/v0.2.1
