export { deepMerge } from 'https://deno.land/std@0.186.0/collections/deep_merge.ts';
export * as preact from 'npm:preact@10.15.0';
export { renderToString } from 'npm:preact-render-to-string@6.1.0';

declare module 'https://deno.land/x/oak@v12.5.0/mod.ts' {
	interface Context {
		/**
		 * Render a Preact component as static HTML
		 * @param component A valid Preact component
		 * @param props Data passed as props for {@link component}
		 */
		render<P>(component: preact.ComponentType<P>, props?: P): void;
	}
}

export * as oak from 'https://deno.land/x/oak@v12.5.0/mod.ts';
